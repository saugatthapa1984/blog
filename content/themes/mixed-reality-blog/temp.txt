


//THis is the lower section of the blog

  <div class="box1">
    <div class="row container">
      <div class="col-lg-6">
        <h1 class="container ml-50 p-2">Editors pick</h1>
        <section class="posts">
          {{#foreach posts limit="1"}}
          <article>
            {{#if feature_image}}
            <div class="main-image">
              <header>
                <a href="{{url}}" class="image fit article-main-image"><img src="{{img_url feature_image}}"
                    class="main-image post-card shadow mb-1" id="article-main-image" alt="{{title}}" />
                </a>
            </div>
            {{/if}}
            <a class="post-card-content-link main-image-content" href="{{url}}">
              <header class="post-card-header">
                {{#if primary_tag}}
                <span class="post-card-tags">{{primary_tag.name}}</span>
                {{/if}}
                <h2 class="post-card-title">{{title}}</h2>
              </header>
              <section class="post-card-excerpt">
                <p>{{excerpt words="33"}}</p>
              </section>
            </a>
          </article>
          {{/foreach}}
        </section>
      </div>

      <div class="col-lg-6">
        <section class="posts">
          {{#foreach posts limit="1"}}
          <article>
            {{#if feature_image}}
            <div class="main-image">
              <header>
                <a href="{{url}}" class="image fit article-main-image"><img src="{{img_url feature_image}}"
                    class="main-image post-card shadow mb-1" id="article-main-image" alt="{{title}}" />
                </a>
            </div>
            {{/if}}
            <a class="post-card-content-link main-image-content" href="{{url}}">
              <header class="post-card-header">
                <h2 class="post-card-title">{{title}}</h2>
              </header>
            </a>
          </article>
          {{/foreach}}
        </section>
      </div>
    </div>
  </div>
